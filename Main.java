import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static int rows;
    static int cols;

    public static void main(String[] args) {

        List<String> data = new ArrayList<String>();
        try {
            File myObj = new File(args[0]);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data.add(myReader.nextLine());
                // System.out.println(data);
            }
            myReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        parseAndPrint(data);
    }

    static void parseAndPrint(List<String> lines) {

        rows = Integer.valueOf(lines.get(0).split("x")[0]);
        cols = Integer.valueOf(lines.get(0).split("x")[1]);
        // System.out.println(lines);

        List<String[]> puzzle = new ArrayList<String[]>();
        for (var i = 0; i < rows; i++) {
            puzzle.add(lines.get(i + 1).split(" "));
        }
        // System.out.println(puzzle)

        List<String> words = new ArrayList<String>();
        for (var i = rows + 1; i < lines.size(); i++) {
            words.add(lines.get(i).replace(" ", ""));
        }
        // System.out.println(words)

        for (String word : words) {
            var sln = findSolution(puzzle, word);
            System.out.println(word + " " + sln);
        }
    }

    static String findSolution(List<String[]> puzzle, String word) {
        String solution = checkHorizontal(puzzle, word);
        if (solution != null)
            return solution;

        solution = checkVertical(puzzle, word);
        if (solution != null)
            return solution;

        solution = checkDiagonal(puzzle, word);
        if (solution != null)
            return solution;

        return "NOT FOUND";
    }

    static String checkHorizontal(List<String[]> puzzle, String word) {

        for (int y = 0; y < rows; y++) {
            // CHECK FORWARD
            for (int startX = 0; startX < cols; startX++) {

                for (int i = 0; i < word.length(); i++) {
                    int x = startX + i;
                    // System.out.println("checking: " + x + ", " + y)

                    if (x >= cols || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x])) {
                        // System.out.println("miss: " + String.valueOf(word.charAt(i)) + " vs " +
                        // puzzle.get(y)[x]);
                        break;
                    } else if (i == word.length() - 1) {
                        // FOUND IT
                        return y + ":" + startX + " " + y + ":" + x;
                    }
                }
            }

            // CHECK BACKWARD
            for (var startX = cols - 1; startX >= 0; startX--) {

                for (var i = 0; i < word.length(); i++) {
                    var x = startX - i;
                    if (x < 0 || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x]))
                        break;
                    else if (i == word.length() - 1) {
                        // FOUND IT
                        return y + ":" + startX + " " + y + ":" + x;
                    }
                }
            }
        }

        // COULDN'T FIND IT
        return null;
    }

    static String checkVertical(List<String[]> puzzle, String word) {

        for (var x = 0; x < cols; x++) {
            // CHECK DOWN
            for (var startY = 0; startY < rows; startY++) {

                for (var i = 0; i < word.length(); i++) {
                    var y = startY + i;

                    if (y >= rows || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x]))
                        break;
                    else if (i == word.length() - 1) {
                        // FOUND IT
                        return startY + ":" + x + " " + y + ":" + x;
                    }
                }
            }

            // CHECK UP
            for (var startY = rows - 1; startY >= 0; startY--) {

                for (var i = 0; i < word.length(); i++) {
                    var y = startY - i;
                    if (y < 0 || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x]))
                        break;
                    else if (i == word.length() - 1) {
                        // found it
                        return startY + ":" + x + " " + y + ":" + x;
                    }
                }
            }
        }

        // COULDN'T FIND IT
        return null;
    }

    static String checkDiagonal(List<String[]> puzzle, String word) {

        // CHECK TOP-LEFT TO BOTTOM-RIGHT
        for (int startX = 0; startX < cols; startX++) {
            for (var startY = 0; startY < rows; startY++) {
                for (int i = 0; i < word.length(); i++) {
                    int x = startX + i;
                    int y = startY + i;

                    if (x >= cols || y >= rows || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x]))
                        break;
                    else if (i == word.length() - 1) {
                        // FOUND IT
                        return startY + ":" + startX + " " + y + ":" + x;
                    }
                }
            }
        }

        // CHECK BOTTOM-RIGHT TO TOP-LEFT
        for (int startX = cols - 1; startX >= 0; startX--) {
            for (int startY = rows - 1; startY >= 0; startY--) {
                for (int i = 0; i < word.length(); i++) {
                    int x = startX - i;
                    int y = startY - i;

                    if (x < 0 || y < 0 || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x]))
                        break;
                    else if (i == word.length() - 1) {
                        // FOUND IT
                        return startY + ":" + startX + " " + y + ":" + x;
                    }
                }
            }
        }

        // CHECK TOP-RIGHT TO BOTTOM-LEFT
        for (int startX = cols - 1; startX >= 0; startX--) {
            for (int startY = 0; startY < rows; startY++) {
                for (int i = 0; i < word.length(); i++) {
                    int x = startX - i;
                    int y = startY + i;

                    if (x < 0 || y >= rows || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x]))
                        break;
                    else if (i == word.length() - 1) {
                        // FOUND IT
                        return startY + ":" + startX + " " + y + ":" + x;
                    }
                }
            }
        }

        // CHECK BOTTOM-LEFT TO TOP-RIGHT
        for (int startX = 0; startX < cols; startX++) {
            for (int startY = rows - 1; startY >= 0; startY--) {
                for (int i = 0; i < word.length(); i++) {
                    int x = startX + i;
                    int y = startY - i;

                    if (x >= cols || y < 0 || !String.valueOf(word.charAt(i)).equals(puzzle.get(y)[x]))
                        break;
                    else if (i == word.length() - 1) {
                        // FOUND IT
                        return startY + ":" + startX + " " + y + ":" + x;
                    }
                }
            }
        }

        // COULDN'T FIND IT
        return null;
    }
}