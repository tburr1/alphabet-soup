# Alphabet Soup

## Prerequisites
* [**Java**](https://www.java.com/en/download/)

## To run
Open this project in Visual Studio Code. Edit launch.json, replacing "sample1" with the name of the ASCII file that you want to test. There are four test files included in this project (see sample1-4 in the root directory.) 

Start the Program by pressing Ctrl+F5, or click on the Run menu -> Run Without Debugging.

Please reach out if you have questions or concerns, my email address is tim.r.burr@gmail.com.